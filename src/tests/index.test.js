import React from "react";
import { mount } from "enzyme";
import enzymeConfig from "../../enzymeConfig";

import EventForm from "../components/Form";

const date = new Date();
const events = [];
const weeks = [[new Date(), new Date(), new Date()]]
const onChangeWeek = () => true;

describe("Inputs Event Form", function() {
  it("Should capture event name correctly onChange", function() {
    const component = mount(<EventForm
      date={date} events={events} weeks={weeks} onChangeWeek={onChangeWeek}/>);
    const input = component.find("textarea").at(0);
    input.instance().value = "Event name";
    input.simulate("change");
    expect(component.state().name).toEqual("Event name");
  });

  it("Should capture event location correctly onChange", function() {
    const component = mount(<EventForm
      date={date} events={events} weeks={weeks} onChangeWeek={onChangeWeek}/>);
    const input = component.find("input").at(0);
    input.instance().value = "Bogotá";
    input.simulate("change");
    expect(component.state().location).toEqual("Bogotá");
  });

  it("Should capture event time correctly onChange", function() {
    const component = mount(<EventForm
      date={date} events={events} weeks={weeks} onChangeWeek={onChangeWeek}/>);
    const input = component.find("input").at(1);
    input.instance().value = "15:00";
    input.simulate("change");
    expect(component.state().time).toEqual("15:00");
  });

  it("Should capture event color correctly onChange", function() {
    const component = mount(<EventForm
      date={date} events={events} weeks={weeks} onChangeWeek={onChangeWeek}/>);
    const input = component.find("input").at(2);
    input.instance().value = "#ffffff";
    input.simulate("change");
    expect(component.state().color).toEqual("#ffffff");
  });

  it("Should add new event on the day", function() {
    const component = mount(<EventForm
      date={date} events={events} weeks={weeks} onChangeWeek={onChangeWeek}/>);

    const inputName = component.find("textarea").at(0);
    inputName.instance().value = "Event name";
    inputName.simulate("change");

    const inputLocation = component.find("input").at(0);
    inputLocation.instance().value = "Bogotá";
    inputLocation.simulate("change");

    const inputTime = component.find("input").at(1);
    inputTime.instance().value = "16:00";
    inputTime.simulate("change");

    const inputColor = component.find("input").at(2);
    inputColor.instance().value = "#ffffff";
    inputColor.simulate("change");

    component.find("form").simulate("submit");
    expect(component.props().events).toEqual([{"color": "#ffffff",
      "location": "Bogotá", "name": "Event name", "time": "16:00"}]);
  });
});