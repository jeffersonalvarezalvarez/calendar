import React, { Component } from "react";

class Event extends Component {
  render() {
    const { event } = this.props;
    return <li>
      <ul>
        <li>{event.name}</li>
        <li>{event.date}</li>
      </ul>
    </li>;
  };
};

export default Event;
