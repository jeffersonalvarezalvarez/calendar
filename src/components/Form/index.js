import moment from "moment";
import React, { Component } from "react";

const INITIAL_STATE = {
  name     : "",
  location : "",
  time     : "",
  color    : ""
};

class EventForm extends Component {
  constructor(props) {
    super(props);

    this.state = INITIAL_STATE;
  };

  sortEvents = events => {
    return events.sort((a, b) => {
        if(parseInt(a.time.split(":")[0]) - parseInt(b.time.split(":")[0]) === 0) {
          return parseInt(a.time.split(":")[1]) - parseInt(b.time.split(":")[1]);
        } else {
          return parseInt(a.time.split(":")[0]) - parseInt(b.time.split(":")[0]);
        }
    });
  };

  onSubmit = (event, weeks, onChangeWeek) => {
    event.preventDefault();
    this.props.events.push(this.state);
    onChangeWeek(weeks);
    this.setState({...INITIAL_STATE});
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { date, events, weeks, onChangeWeek } = this.props;
    const { name, location, color, time } = this.state;

    return <div
      className="modal fade"
      id={"modal"+(date.getDate()+date.getMonth()+date.getFullYear())}
      tabIndex="-1"
      role="dialog"
      aria-labelledby={(date.getDate()+date.getMonth()+date.getFullYear())+"Label"}
      aria-hidden="true">
      <div
        className="modal-dialog"
        role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title"
              id={(date.getDate()+date.getMonth()+date.getFullYear())+"Label"}>
              {moment(date).format('ll')}
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <form onSubmit={(event) => this.onSubmit(event, weeks, onChangeWeek)}>
              <h3>Add event</h3>
              <div className="form-group">
                <div className="row">
                  <div className="col-md-2">
                    <label htmlFor="name">
                      Event's name
                    </label>
                  </div>
                  <div className="col-md-10">
                    <textarea
                      className="form-control"
                      id="name"
                      name="name"
                      type="text"
                      placeholder="Event's name"
                      maxLength="30"
                      onChange={this.onChange}
                      value={name}
                      required/>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="row">
                  <div className="col-md-2">
                    <label htmlFor="location">
                      Location
                    </label>
                  </div>
                  <div className="col-md-10">
                    <input
                      className="form-control"
                      id="location"
                      name="location"
                      type="text"
                      placeholder="Location"
                      onChange={this.onChange}
                      value={location}
                      required/>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="row">
                  <div className="col-md-2">
                    <label htmlFor="time">
                      Time
                    </label>
                  </div>
                  <div className="col-md-10">
                    <input
                      className="form-control"
                      id="time"
                      name="time"
                      type="time"
                      placeholder="Time"
                      onChange={this.onChange}
                      value={time}
                      required/>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="row">
                  <div className="col-md-2">
                    <label htmlFor="color">
                      Color
                    </label>
                  </div>
                  <div className="col-md-10">
                    <input
                      className="form-control"
                      id="color"
                      name="color"
                      type="color"
                      placeholder="Color"
                      onChange={this.onChange}
                      value={color}
                      required/>
                  </div>
                </div>
              </div>
              <div className="row float-right">
                <div className="col-md-12 ">
                  <button
                    type="submit"
                    className="btn btn-primary">
                    Save
                  </button>
                </div>
              </div>
            </form>
            <br /><br />
            <div className="row">
              <div className="col-md-12">
                <h3>Current events</h3>
              </div>
              <div className="col-md-12">
                {
                  events.length === 0 ?
                  <p>There is no events for this date</p> :
                  null
                }
                <ul className="list-group">
                {
                  this.sortEvents(events).map((event, idx) => {
                    return <li
                      key={idx}
                      className="list-group-item"
                      style={{
                        backgroundColor: event.color
                      }}>
                      {event.name} in {event.location} at {event.time}
                    </li>
                  })
                }
                </ul>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal">
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  };
};

export default EventForm;
