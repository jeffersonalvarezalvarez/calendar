import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";

import * as constantsCalendar from "../../constants/actions/Calendar";

const initialState = {
  weeks: []
};

function rootReducer(state, action) {
  switch(action.type) {
    case constantsCalendar.RETRIEVE_LISTS_SUCCESS:
      return {
        weeks: action.json.weeks
      };
    default:
      return state;
  }
};

export default function configureStore() {
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      thunk
    )
  );
  return store;
};
