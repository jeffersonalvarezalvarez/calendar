import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import Week from "../Date/Week";

import * as constantsCalendar from "../../constants/actions/Calendar";

/**
  * @param {int} The month number, 0 based
  * @param {int} The year, not zero based, required to account for leap years
  * @return {Date[]} List with date objects for each day of the month
*/
function getDaysAndEvents(month, year) {
  return dispatch => {
    return fetch("https://pro.openweathermap.org/data/2.5/climate/month?q=Bogota&appid=b1b15e88fa797225412429c1c50c122a1")
    .then(response => {
      if(response.status !== 200) {
        throw new Error("We can't adquire weather data");
      }
      return response.json(); })
    .then(json => {
      let days = [];
      let init = null;
      let date = new Date(Date.UTC(year, month, 1));
      const temps = json.list;
      const curDate = new Date();
      for(let i = 0; i < 35; i++) {
        let day = {   
          date   : new Date(date),
          events : []
        };
        // Temps greater than today
        if((date.getMonth() === curDate.getMonth()) &&
           (date.getDate() > curDate.getDate())){
          if(!init) init = i;
          if(!!temps[i-(init-1)]) {
            day.weather = temps[i-(init-1)];
          }
        }
        days.push(day);
        date.setDate(date.getDate() + 1);
      }
      console.log(days);
      dispatch({
        type : constantsCalendar.RETRIEVE_LISTS_SUCCESS,
        json : {
          weeks: _.chunk(days, 7)
        }
      }); })
    .catch(error => {
      dispatch({
        type : constantsCalendar.RETRIEVE_LISTS_ERROR,
        json : {
          error: error
        }
      });
    });
  }
};

class Calendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      weeks: []
    };
  };

  onChangeWeek = weeks => {
    this.setState({ weeks: weeks });
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.weeks !== prevState.weeks) {
      return ({ weeks: nextProps.weeks });
    }
    return ({ weeks: prevState.weeks });
  }

  componentDidMount() {
    this.props.getDaysAndEvents(9, 2019);
  };

  render() {
    const { weeks } = this.state;

    return <div className="container">
      <h1><center>October</center></h1>
      <div className="row">
        <div className="col-sm">
          <div className="card">
            <div className="card-body">
              Monday
            </div>
          </div>
        </div>
        <div className="col-sm">
          <div className="card">
            <div className="card-body">
              Tuesday
            </div>
          </div>
        </div>
        <div className="col-sm">
          <div className="card">
            <div className="card-body">
              Wednesday
            </div>
          </div>
        </div>
        <div className="col-sm">
          <div className="card">
            <div className="card-body">
              Thursday
            </div>
          </div>
        </div>
        <div className="col-sm">
          <div className="card">
            <div className="card-body">
              Friday
            </div>
          </div>
        </div>
        <div className="col-sm">
          <div className="card">
            <div className="card-body">
              Saturday
            </div>
          </div>
        </div>
        <div className="col-sm">
          <div className="card">
            <div className="card-body">
              Sunday
            </div>
          </div>
        </div>
      </div>        
      { weeks.map((week, idx) => <Week key={idx} week={week} weeks={weeks} onChangeWeek={this.onChangeWeek} />) }
    </div>
  };
};

const structuredSelector = createStructuredSelector({
  weeks: state => state.weeks
});

const mapDispatchToProps = {
  getDaysAndEvents
};

export default connect(structuredSelector, mapDispatchToProps)(Calendar);