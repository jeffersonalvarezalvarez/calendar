import React, { Component } from "react";

import Day from "./Day";

class Week extends Component {
  render() {
    const { week, weeks, onChangeWeek } = this.props;
    return <div className="row">
      {week.map((day, idx) => <Day key={idx} day={day} weeks={weeks} onChangeWeek={onChangeWeek}/>)}
    </div>
  };
};

export default Week;
