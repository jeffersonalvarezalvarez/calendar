import React, { Component } from "react";

import EventForm from "../Form";

class Day extends Component {
  render() {
    const { day, weeks, onChangeWeek } = this.props;

    return <div className="col-sm">
      <div className={day.events.length > 0 ? "card bg-info" : "card"}>
        <div className="card-body">
          <div className="row float-right">
            <div className="col-md-12">
              <p><b>{day.date.getDate()}</b></p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <li>
                <i class="fas fa-water"></i> {!!day.weather ? day.weather.humidity : null}
              </li>
              <li>
                <i class="fas fa-weight"></i> {!!day.weather ? day.weather.pressure : null}
              </li>
              <li>
                <i class="fas fa-temperature-low"></i> {!!day.weather ? day.weather.temp.average : null}
              </li>
              <li>
              <i class="fas fa-wind"></i> {!!day.weather ? day.weather.wind_speed : null}
              </li>
            </div>
          </div>
          <button
            type="button"
            className="btn btn-primary"
            data-toggle="modal"
            data-target={"#modal"+(day.date.getDate()+day.date.getMonth()+day.date.getFullYear())}>
            <i className="fa fa-plus" aria-hidden="true"></i>
          </button>
          <EventForm date={day.date} events={day.events} weeks={weeks} onChangeWeek={onChangeWeek}/>
        </div>
      </div>
    </div>
  };
};

export default Day;
