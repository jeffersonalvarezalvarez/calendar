import "./styles/styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

import "jquery/dist/jquery.min.js";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "@fortawesome/fontawesome-free/js/all.min.js";

import React from "react";
import ReactDOM from "react-dom";

import Calendar from "./components/Calendar";
import { Provider } from "react-redux";

import * as serviceWorker from "./serviceWorker";
import calendarStore from "../src/components/Calendar/actions";

ReactDOM.render(
  <Provider store={calendarStore()}>
    <Calendar />
  </Provider>,
  document.getElementById("root"),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
